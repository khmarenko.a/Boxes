package org.example.Boxing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BoxingByIf {

    public static void addArray(List<int[]> list, int high, int weigh) {
        list.add(new int[]{high, weigh});
    }

    public static void print(List<int[]> list, boolean result) {
        for (int i = 0; i < list.size(); i++) {
            System.out.print(Arrays.toString(list.get(i)));
            if (i < list.size() - 1 && result) System.out.print("->");
        }
        System.out.println();
    }

    private static List<int[]> collectBoxes(List<int[]> list) {

        List<int[]> outcome = new ArrayList<>();

        int[] max = list.get(0);

        for (int[] value : list) {
            if ((value[0] > max[0] && value[1] > max[1])
                    || (value[1] > max[0] && value[0] > max[1])) {
                max = value;
            }
        }
        outcome.add(max);
        list.remove(max);

        for (int[] ints : list) {
            for (int j = 0; j < outcome.size(); j++) {
                if (
                        (ints[0] == outcome.get(j)[0] && ints[1] < outcome.get(j)[1] / 2)
                                || (ints[1] == outcome.get(j)[1] && ints[0] < outcome.get(j)[0] / 2)
                ) {
                    outcome.add(j, ints);
                    break;
                }

                if (
                        (ints[0] == outcome.get(j)[0] && ints[1] < outcome.get(j)[1] * 2)
                                || (ints[1] == outcome.get(j)[1] && ints[0] < outcome.get(j)[0] * 2)
                                || (ints[0] == outcome.get(j)[1] && ints[1] > outcome.get(j)[0] / 2)
                                || (ints[1] == outcome.get(j)[0] && ints[0] < outcome.get(j)[1] / 2)
                                || (ints[0] == outcome.get(j)[0] && ints[0] > outcome.get(j)[0] / 2)
                                || (ints[0] == outcome.get(j)[1] && ints[1] < outcome.get(j)[1] / 2)
                                || (ints[0] >= outcome.get(j)[0] && ints[1] < outcome.get(j)[1])
                                || (ints[1] >= outcome.get(j)[1] && ints[0] < outcome.get(j)[0])
                ) {
                    break;
                }

                if (
                        (ints[0] < outcome.get(j)[0] && ints[1] < outcome.get(j)[1])
                                || (ints[1] < outcome.get(j)[1] && ints[0] < outcome.get(j)[0])
                                || (ints[1] < outcome.get(j)[0] && ints[0] < outcome.get(j)[1])
                                || (ints[0] < outcome.get(j)[1] && ints[1] < outcome.get(j)[0])) {
                    outcome.add(j, ints);
                    break;
                }
            }
        }
        return outcome;
    }

    public static void boxingByIf(List<int[]> list) {
        if (list.size() == 0) {
            System.out.print("Комплектация завершена.");
            return;
        }

        if (list.size() == 1) {
            System.out.print("не комплектуется:       ");
            print(list, false);
            return;
        }
        System.out.print("Исходные данные:        ");
        print(list, false);
        System.out.print("Результат комплектации: ");
        List<int[]> outcome = collectBoxes(list);
        print(outcome, true);
        System.out.println();
        list.removeAll(outcome);
        boxingByIf(list);
    }
}