package org.example.Boxing;

import org.example.XGraph.GraphTraverse;
import org.example.XGraph.XGraph;

import java.util.*;
import java.util.stream.Stream;

import static org.example.Boxing.BoxingByIf.print;

public class BoxingByGraph {

    public static GraphTraverse getGraphTraverse(List<int[]> list) {
        XGraph g = new XGraph(list.size());

        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.size(); j++) {
                if (list.get(i)[0] == list.get(j)[0] && list.get(i)[1] == list.get(j)[1]
                        || list.get(i)[0] == list.get(j)[1] && list.get(i)[1] == list.get(j)[0]) continue;
                if (list.get(i)[0] < list.get(j)[0] && list.get(i)[1] < list.get(j)[1]
                        || list.get(i)[0] < list.get(j)[1] && list.get(i)[1] < list.get(j)[0]
                ) {
                    g.add(i, j);
                }
            }
        }
        return new GraphTraverse(g);
    }

    public static void boxingByGraph(List<int[]> incomelist) {

        if (incomelist.size() == 0) {
            System.out.print("Комплектация завершена.");
            return;
        }

        List<int[]> list = new ArrayList<>(incomelist);
        GraphTraverse g = getGraphTraverse(list);

        List<List<Integer>> outcome = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            outcome.add(longWayByValue(g, i));
        }

        List<Integer> result = outcome.stream()
                .sorted((list1, list2) -> {
                    int length1 = list1.size();
                    int length2 = list2.size();
                    return Integer.compare(length2, length1);
                }).toList().get(0);

//        System.out.println("result: " + result);//////////////////////

        List<int[]> resultList = new ArrayList<>();
        for (Integer integer : result) {
            resultList.add(list.get(integer));
        }

        System.out.print("Исходные данные:        ");
        print(list, false);

        System.out.print("Результат комплектации: ");
        print(resultList, true);

        System.out.println();
        list.removeAll(resultList);

        boxingByGraph(list);
    }

    private static List<Integer> longWayByValue(GraphTraverse g, int value) {


        List<List<Integer>> sortedList = Stream.of(value)
                .flatMap(i -> g.dfs(i)
                        .stream()
                        .map(g::dfs))
                .sorted((list1, list2) -> {
                    int length1 = list1.size();
                    int length2 = list2.size();
                    return Integer.compare(length2, length1);
                }).toList();

        //System.out.println("sortedList " + sortedList);

        int num = 0;
        List<Integer> temp = new ArrayList<>(sortedList.get(0));
        List<Integer> longestWay = new ArrayList<>();

        for (List<Integer> list : sortedList) {
            if (list.size() == num) continue;
            if (!temp.contains(list.get(0))) continue;
            longestWay.add(list.get(0));
            num = list.size();
            temp = list;
        }
//        System.out.println("LW :" + longestWay);
        return longestWay;
    }
}