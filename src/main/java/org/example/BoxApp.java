package org.example;

import java.util.*;

import static org.example.Boxing.BoxingByGraph.*;
import static org.example.Boxing.BoxingByIf.*;

public class BoxApp {
    public static void main(String[] args) {

        List<int[]> list = new ArrayList<>();

        addArray(list, 3, 5);     // 0
        addArray(list, 2, 4);     // 1
        addArray(list, 5, 1);     // 2
        addArray(list, 2, 5);     // 3
        addArray(list, 6, 4);     // 4
        addArray(list, 8, 5);     // 5
        addArray(list, 2, 1);     // 6
        addArray(list, 7, 5);     // 7
        addArray(list, 5, 7);     // 8
        addArray(list, 5, 12);    // 9

        addArray(list, 2, 14);      // 10
        addArray(list, 18, 14);     // 11
        addArray(list, 22, 36);     // 12
        addArray(list, 23, 21);     // 13
        addArray(list, 2, 14);      // 14
        addArray(list, 18, 14);     // 15
        addArray(list, 22, 36);     // 16
        addArray(list, 23, 21);     // 17
        addArray(list, 22, 36);     // 18
        addArray(list, 23, 21);;    // 19

        addArray(list, 99, 30);    // 20
        addArray(list, 99, 20);    // 21
        addArray(list, 98, 21);    // 22
        addArray(list, 48, 48);    // 23
        addArray(list, 50, 50);    // 24
        addArray(list, 49, 49);    // 25
        addArray(list, 97, 22);    // 26
        addArray(list, 96, 23);    // 27
        addArray(list, 95, 24);    // 28
        addArray(list, 94, 25);    // 29

        addArray(list, 54, 14);    // 30
        addArray(list, 85, 14);    // 31
        addArray(list, 24, 36);    // 32
        addArray(list, 96, 21);    // 33
        addArray(list, 35, 14);    // 34
        addArray(list, 25, 14);    // 35
        addArray(list, 64, 36);    // 36
        addArray(list, 67, 21);    // 37
        addArray(list, 34, 36);    // 38
        addArray(list, 67, 21);    // 39

        addArray(list, 54, 46);    // 40
        addArray(list, 85, 87);    // 41
        addArray(list, 24, 32);    // 42
        addArray(list, 96, 43);    // 43
        addArray(list, 35, 96);    // 44
        addArray(list, 25, 57);    // 45
        addArray(list, 64, 85);    // 46
        addArray(list, 67, 21);    // 47
        addArray(list, 34, 34);    // 48
        addArray(list, 67, 76);    // 49

        addArray(list, 46, 46);    // 50
        addArray(list, 82, 87);    // 51
        addArray(list, 46, 32);    // 52
        addArray(list, 87, 43);    // 53
        addArray(list, 35, 96);    // 54
        addArray(list, 19, 57);    // 55
        addArray(list, 65, 85);    // 56
        addArray(list, 58, 21);    // 57
        addArray(list, 73, 34);    // 58
        addArray(list, 21, 76);    // 59

        addArray(list, 46, 76);    // 60
        addArray(list, 82, 98);    // 61
        addArray(list, 46, 15);    // 62
        addArray(list, 87, 67);    // 63
        addArray(list, 35, 16);    // 64
        addArray(list, 19, 34);    // 65
        addArray(list, 65, 76);    // 66
        addArray(list, 58, 75);    // 67
        addArray(list, 73, 83);    // 68
        addArray(list, 21, 26);    // 69

        addArray(list, 55, 76);    // 70
        addArray(list, 82, 98);    // 71
        addArray(list, 25, 15);    // 72
        addArray(list, 36, 67);    // 73
        addArray(list, 14, 16);    // 74
        addArray(list, 48, 34);    // 75
        addArray(list, 25, 76);    // 76
        addArray(list, 39, 75);    // 77
        addArray(list, 77, 83);    // 78
        addArray(list, 11, 26);    // 79

        addArray(list, 55, 88);    // 80
        addArray(list, 82, 16);    // 81
        addArray(list, 25, 28);    // 82
        addArray(list, 36, 65);    // 83
        addArray(list, 14, 27);    // 84
        addArray(list, 48, 54);    // 85
        addArray(list, 25, 83);    // 86
        addArray(list, 39, 28);    // 87
        addArray(list, 77, 54);    // 88
        addArray(list, 11, 11);    // 89

        addArray(list, 22, 88);    // 90
        addArray(list, 43, 16);    // 91
        addArray(list, 35, 28);    // 92
        addArray(list, 76, 65);    // 93
        addArray(list, 91, 27);    // 94
        addArray(list, 45, 54);    // 95
        addArray(list, 55, 83);    // 96
        addArray(list, 88, 28);    // 97
        addArray(list, 68, 54);    // 98
        addArray(list, 94, 11);    // 99







        long d = new Date().getTime();
        System.out.println("--------------------------\nРЕШЕНИЕ ПРИ ПОМОЩИ GRAPH\n");
        boxingByGraph(list);
        System.out.println();
        long e = new Date().getTime();
        System.out.println(e-d);


//        d = new Date().getTime();
//        System.out.println("\n--------------------------\nРЕШЕНИЕ ПРИ ПОМОЩИ IF-ов\n");
//        boxingByIf(list);
//        System.out.println();
//        e = new Date().getTime();
//        System.out.println(e-d);
    }
}