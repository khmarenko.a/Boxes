package org.example.XGraph;

import java.util.LinkedList;
import java.util.List;

public class GraphTraverse {
    private final XGraph g;

    public GraphTraverse(XGraph g) {
        this.g = g;
    }

    public List<Integer> bfs(Integer vertexFrom) {
        LinkedList<Integer> outcome = new LinkedList<>();
        boolean[] visited = new boolean[g.vertexCount];

        LinkedList<Integer> process = new LinkedList<>();
        visited[vertexFrom] = true;
        process.add(vertexFrom);

        while (!process.isEmpty()) {
            int curr = process.poll();
            outcome.add(curr);
            LinkedList<Integer> children = g.getEdgesFrom(curr);
            for (int child : children) {
                if (visited[child]) continue;
                visited[child] = true;
                process.add(child);
            }
        }
        return outcome;
    }

    private List<Integer> dfs(Integer vertexFrom, boolean[] visited, List<Integer> outcome) {
        if (visited[vertexFrom]) return outcome;
        visited[vertexFrom] = true;

        outcome.add(vertexFrom);

        LinkedList<Integer> children = g.getEdgesFrom(vertexFrom);
        for (int ch : children) {
            dfs(ch, visited, outcome);
        }
        return outcome;
    }

    public List<Integer> dfs(Integer vertexFrom) {

        return dfs(vertexFrom, new boolean[g.vertexCount], new LinkedList<>());
    }
}
