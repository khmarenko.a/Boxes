package org.example.XGraph;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class XGraph {
    public final int vertexCount;
    private final List<LinkedList<Integer>> edges;

    public XGraph(int vertexCount) {
        this.vertexCount = vertexCount;
        this.edges = IntStream.range(0,vertexCount)
                .mapToObj(n-> new LinkedList<Integer>())
                .collect(Collectors.toList());
    }

    public void add(int vertexFrom, int vertexTo){
        LinkedList<Integer> to = edges.get(vertexFrom);
        to.add(vertexTo);

    }
    public void remove(int vertexFrom, int vertexTo){
        LinkedList<Integer> to = edges.get(vertexFrom);
        Integer to_boxed = vertexTo;
        to.add(to_boxed);
    }

    public LinkedList<Integer> getEdgesFrom(int vertexFrom){
        return edges.get(vertexFrom);
    }

    public void printGraph(){
        for (int i = 0; i < vertexCount; i++) {
            System.out.print("Node "+i);
            for (int x: edges.get(i)) {
                System.out.print(" -> " + x);
            }
            System.out.println();
        }
    }
}
